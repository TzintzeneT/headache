using GLib;

class QRbuild{

    static int pointer = 0;


    public static void build(uint[,] QR){
        /*QR[21, 21] = 0;
        QR[21, 20] = 1;
        QR[20, 20] = 0;
        QR[20, 21] = 0;*/
        //uint tempData = 10100111;

        //int topData = 9;
        //int minData = 21;
        for(int i = 20, row = 0; i > 10; i -= 2, row++){
            int j = 20;
            int plus = -1;
            if(row%2 == 1){
                j = 9;
                plus = 1;
                print("down\n");
            }
            else{
                print("up\n");
            }
            /*if(j%2 == 1){
                QR[j, i] = 2;//rando();
                QR[j + plus, i] = 3;//rando();
                j += plus;
                QR[j - plus, i-1] = 4;//rando();
                QR[j, i-1] = 5;//rando();
                j += plus;
            }else{
                QR[j + plus, i] = 2;//rando();
                QR[j, i] = 3;//rando();
                j += plus;
                QR[j, i-1] = 4;//rando();
                QR[j - plus, i-1] = 5;//rando();
                j += plus;
            }*/

            /*QR[j + plus, i] = 2 + j%2;//rando();
            QR[j, i] = 3 - j%2;//rando();
            QR[j + plus, i-1] = 4 + j%2;//rando();
            QR[j, i-1] = 5 - j%2;//rando();*/

            QR[j - j%2*plus + plus, i] = 2;
            QR[j + j%2*plus, i] = 3;
            QR[j - j%2*plus + plus, i-1] = 4;
            QR[j + j%2*plus, i-1] = 5;
            j += plus*2;

            for(;j > 10 && j < 19; j += plus){
                QR[j, i] = rando();
                //tempData /= 10;
                QR[j, i-1] = rando(); //tempData%10;
                pointer++;
                //tempData /= 10;
                //tempData = 10100111;
            }
            /*QR[j + plus, i] = 6 + j%2;
            QR[j, i] = 7 - j%2;
            QR[j + plus, i-1] = 8 + j%2;
            QR[j, i-1] = 9 - j%2;*/

            QR[j - j%2*plus + plus, i] = 6;
            QR[j + j%2*plus, i] = 7;
            QR[j - j%2*plus + plus, i-1] = 8;
            QR[j + j%2*plus, i-1] = 9;
        }
    }

    public static uint getData(){

        uint[] TempData = {10101000, 10111101, 11010101, 01010101, 01001011, 10101000, 10111101, 01010101, 01010101, 01001011};
        uint data = TempData[pointer/8];
        print("location: %d, help: %u\n", pointer/8, TempData[3]);
        int cut = (int) Math.pow(10, pointer%8);
        data /= cut;
        data = data%10;
        print("%u, %f\n", data, cut);
        pointer++;



        /*
        int location = pointer/8;
        uint iValue = TempData[location]; //Math.pow(pointer%8, 10);
        //print("what: %f\n", TempData[1]);
        print("what: %u\n", iValue);
        iValue /= 10;
        iValue = iValue%10;
        //Math.pow(pointer%8, 10);
        print("what: %u\n", iValue);
        //iValue = iValue%10;
        print("%u pointer: %d location: %d\n", iValue, pointer, location);
        */
        return data;
    }

    public static uint tesTdata(){
        return pointer%2;
    }
    public static uint rando(){
        return Random.int_range(0, 2);
    }
}

void createPPM(uint[,] QR){
    FileStream fs = FileStream.open("QRcode.ppm", "wb");
    fs.puts("P6\n#Created with TzinQR\n21 21\n255\n");
    uint8[] data = new uint8[21*21*3];
    for(int i = 0, dataptr = 0; i < QR.length[0]; i++){
        for(int j = 0; j < QR.length[1]; j++, dataptr += 3){
            data[dataptr] = (uint8) QR[i, j] - 1;
            data[dataptr+1] = (uint8) QR[i, j] - 1;
            data[dataptr+2] = (uint8) QR[i, j] - 1;
        }
    }
    /*foreach (var pixel in QR){
        data[i] = color * 255;
        data[i+1] = color * 255;
        data[i+2] = color * 255;
    }*/
    fs.write(data, 1);

}

void addDefData(uint[,] QR){

    for(int i = 0; i < 7; i++){
        QR[0, i] = 1;
        QR[i, 0] = 1;
        QR[6, i] = 1;
        QR[i, 6] = 1;

        QR[0, QR.length[0] - i - 1] = 1;
        QR[i, QR.length[0] - 7] = 1;
        QR[6, QR.length[0] - i - 1] = 1;
        QR[i, QR.length[0] - 1] = 1;

        QR[QR.length[1] - 1, i] = 1;
        QR[QR.length[1] - i - 1, 0] = 1;
        QR[QR.length[1] - 7, i] = 1;
        QR[QR.length[1] - i - 1, 6] = 1;
    }

    for(int i = 2; i < 5; i++){

        for(int j = 2; j < 5; j++){
            QR[i, j] = 1;
            QR[QR.length[0] - i - 1, j] = 1;
            QR[i, QR.length[1] - j - 1] = 1;
        }
    }

    for(int i = 8; i < QR.length[0] -7; i += 2){
        QR[i, 6] = 1;
        QR[6, i] = 1;
    }
}

void addFormatData(uint[,] QR){
    uint[,] fData = QR.copy(); //new uint[QR.length[0], QR.length[1]];
        /*{
        {0, 1},
        {1, 1}
        };*/
        //Array (new int[5], 2);

        uint[,] dataType = {{0, 0}, {1, 0}};

        for(int i = 0; i < dataType.length[0]; i++){
            for(int j = 0; j < dataType.length[1]; j++){
                fData[fData.length[0] - dataType.length[0] + i, fData.length[1] - dataType.length[1] + j] = dataType[i, j];
                //fData[i, j] = dataType[i, j];
            }
        }

        uint[,] otherData = {
            {0, 1, 1, 0, 1, 0, 2, 1, 0, 2, 2, 2, 2, 0, 1, 1, 1, 0, 1, 1, 0},
            {0, 1, 1, 0, 1, 1, 2, 1, 0, 2, 2, 2, 2, 1, 1, 0, 1, 0, 1, 1, 0}
        };

        for(int i = 0; i < otherData.length[1]; i++){
            if(otherData[0, i] != 2){
                fData[8, i] = otherData[0, i];
            }
            if(otherData[1, i] != 2){
                fData[i, 8] = otherData[1, i];
            }
        }

        //fData[fData.length[0]-1, fData.length[1]-1] = 0;


        for(int i = 0; i < fData.length[1]; i++){
            for(int j = 0; j < fData.length[1]; j++){
                QR[i, j] = fData[i, j];
            }
        }
}

void main() {

    FileStream fs = FileStream.open("qr2.ppm", "wb");
    uint8[] data = {104, 106}, data2 = null;

    fs.write(data, 1);

        //file_stream.read(data2, 1);
        //data2[1] = 105;
        //print("%u\n", data2[0]);
    fs.puts("meow");

    print("sus\n");
        //stdout.write(data, 1);

    uint[,] QR = new uint[21, 21];
    QRbuild.build(QR);
    addDefData(QR);
    addFormatData(QR);
    for(int i = 0; i < 21; i++){
        for(int j = 0; j < 21; j++){
            print("%u ", QR[i, j]);
        }
        print("\n");
    }
    print("hello?\n");
    createPPM(QR);

    /*for(int i = 0; i < 30; i++){
        print("test\n");
        QRbuild.tesT();
        print("ha\n");
    }

    QRbuild.getData();
    QRbuild.getData();*/
}
